#!/bin/bash -xe
apt-get install git -y
apt-get install python3 -y
apt-get install python3-pip -y
git clone https://gitlab.com/nicolasumaras/api_projeto_dev
cd api_projeto_dev
pip install -r requirements.txt
gunicorn api:app -c gunicorn.conf.py