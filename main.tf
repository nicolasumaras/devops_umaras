# ------------------------------------------------------------------------------
# CONFIGURAÇÃO DO PROVIDER - VARIÁVEIS ENTREGUES ATRAVÉS DAS VARIÁVEIS
# DE AMBIENTE DO GITLAB
# ------------------------------------------------------------------------------
provider "google" {
  region  = var.region
  project = var.project
  credentials = var.chave_gcp
}

provider "google-beta" {
  region  = var.region
  project = var.project
  credentials = var.chave_gcp
}

terraform {
  required_providers {
    google = {
      version = "~> 3.83.0"
    }
    google-beta = {
      version = "~> 3.83.0"
    }
  }
    backend "http" {
    }
}

resource "google_project_service" "gcp_apis" {
  for_each = toset(var.gcp_service_list)
  service = each.key
}

# ------------------------------------------------------------------------------
# CAMINHO PARA O SCRIPT DE STARTUP DA IMAGEM DE VM
# ------------------------------------------------------------------------------

data "template_file" "instance_startup_script" {
  template = file("${path.module}/templates/gceme.sh.tpl")

  vars = {
    PROXY_PATH = ""
  }
}

# ------------------------------------------------------------------------------
# CRIANDO A IMAGEM DE INSTÂNCIA 
# ------------------------------------------------------------------------------

resource "google_service_account" "instance-group" {
  account_id = "grupo-de-instancias"
}

module "instance_template" {
  source               = "terraform-google-modules/vm/google//modules/instance_template"
#  version              = "~> 6.2.0"
  subnetwork           = google_compute_subnetwork.subnetwork.self_link
  source_image_family  = var.image_family
  source_image_project = var.image_project
  startup_script       = data.template_file.instance_startup_script.rendered
  tags = tolist([var.port_name])

  service_account = {
    email  = google_service_account.instance-group.email
    scopes = ["cloud-platform"]
  }
}

# ------------------------------------------------------------------------------
# CRIANDO A INSTÂNCIA DE GRUPO
# ------------------------------------------------------------------------------

module "managed_instance_group" {
  source            = "terraform-google-modules/vm/google//modules/mig"
#  version           = "~> 6.2.0"
  region            = var.region
  target_size       = 1
  hostname          = "instancias-gerenciadas"
  instance_template = module.instance_template.self_link

  target_pools = [
    module.load_balancer.target_pool,
  ]

  named_ports = [{
    name = var.port_name
    port = var.port_number
  }]

}

# ------------------------------------------------------------------------------
# CRIANDO O BALANCEADOR DE CARGA
# ------------------------------------------------------------------------------

module "load_balancer" {
  source       = "GoogleCloudPlatform/lb/google"
  name         = "balanceador-carga"
  region       = var.region
  service_port = var.port_number
  target_tags  = [var.port_name,]
  network      = google_compute_network.network.name

  health_check = {
    "check_interval_sec": null,
    "healthy_threshold": null,
    "host": null,
    "port": var.port_number,
    "request_path": "/api/comment/list/1",
    "timeout_sec": null,
    "unhealthy_threshold": null
  }

}