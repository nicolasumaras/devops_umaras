resource "google_compute_network" "network" {
  name                    = "rede-balanceador"
  auto_create_subnetworks = "false"
}

resource "google_compute_subnetwork" "subnetwork" {
  name          = "subnet-balanceador"
  region        = var.region
  network       = google_compute_network.network.self_link
  ip_cidr_range = "10.0.0.0/16"
}

resource "google_compute_router" "router" {
  name    = "router-balanceador"
  region  = var.region
  network = google_compute_network.network.self_link
}

module "cloud_nat" {
  project_id = var.project
  region     = var.region
  name       = "nat-balanceador"
  source     = "terraform-google-modules/cloud-nat/google"
#  version    = "~> 1.0.0"
  router     = google_compute_router.router.name
}