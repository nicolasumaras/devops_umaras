variable "chave_gcp" {
  type = string
}

variable "project" {
  type = string
}

variable "region" {
  type = string
}

variable "gcp_service_list" {
  description ="lista de APIs para o projeto"
  type = list(string)
  default = [
    "iam.googleapis.com",
    "compute.googleapis.com"
  ]
}

# ------------------------------------------------------------------------------
# PARÂMETROS DA IMAGEM DE INSTÂNCIA 
# ------------------------------------------------------------------------------

variable "image_family" {
  type = string
  description = "Image used for compute VMs."
  default     = "debian-11"
}

variable "image_project" {
  type = string
  description = "GCP Project where source image comes from."
  default     = "debian-cloud"
}

# ------------------------------------------------------------------------------
# PARÂMETROS PARA CRIAÇÃO DA REGRA DE FIREWALL PARA UTILIZAÇÃO DA API
# ------------------------------------------------------------------------------

#NOME DA PORTA: PARA CONFIGURAÇÃO DE TAGS
variable "port_name" {
  type = string
  description = "nome da porta que será utilizada pela api"
  default = "api"
}

#NUMERO DA PORTA PARA CONFIGURAÇÃO DE FIREWALL E LOAD BALANCER
variable "port_number" {
  type = string
  description = "porta utilizada pela API"
  default = "8000"
}