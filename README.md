# Obejtivo

>Este projeto traz um script de Terraform para realizar o deploy da API na Google Cloud Platform. A API está localizada na pasta resources. Serão criados uma imagem de VM, um grupo de instâncias, um balanceador de carga e recursos de rede e regras de firewall.

<img  src= "https://storage.googleapis.com/gcp-community/tutorials/modular-load-balancing-with-terraform/example-lb-diagram.png">

## 💻 Pré-requisitos


* Conta e projeto criados na GCP
* Conta de serviço e chave geradas para o Terraform na GCP
* Ativação da Cloud Resource Manager API

## Preparando o ambiente deploy:

* Colocar a chave de serviço para o Terraform como a vairável de ambiente TF_VAR_chave_gcp no Gitlab
* Colocar nome do projeto na GCP como a vairável de ambiente TF_VAR_project no Gitlab
* Colocar a região da GCP como a vairável de ambiente TF_VAR_region no Gitlab

## Variáveis do Terraform:
* Valores default já estão configurados para o comissionamento
