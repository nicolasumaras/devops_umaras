## Considerações:

A aplicação armazena os comentários em variáveis locais, desta forma a decisão foi por utilizar uma VM com Debian e um script de provisionamento que instala os requerimentos e excecuta a aplicação via um script de startup. 

Foi adicionado um load balancer simples para demonstrar o que seria um cenário ideal de operação para um sistema que precisa de escalonamento. O número de instâncias está limitado em uma por conta do armazenamento local das variáveis. Com um número maior de instâncias o load balancer enviaria certos pedidos para máquinas aleatórias, então nem sempre um cliente receberia a mesma resposta da API e os comentários postados ficariam distribuidos entre as instâncias do cluster. Caso o gerenciamento de instâncias detectasse uma máquina funcionando de forma errática e decidisse substituir a instância, alguns comentários seriam perdidos, desta forma o limite de uma instância foi estabelecido e as configurações para escalonamento e substituição automática de instâncias não foram habilitadas.

Com essas considerações básicas, a Google Cloud Platform foi escolhida como provedora devido a maior familiaridade com os serviços ofertados por conta do treinamento oferecido pela Globo/Pluralsight para os times após a parceria fechada este ano. Para a parte de IaC o Terraform foi escolhido e para criar uma pipeline básica o GitLab foi utilizado. Da forma que está configurado o cache do Terraform também fica armazenado no Gitlab. Durante o primeiro teste de comissionamento o Gitlab não foi utilizado e no seu lugar foi criada uma VM dentro da GCP com o VScode e Terraform para facilitar o acesso.

Para comissionamento da máquina foi criado um script bash (gceme.sh.tpl) que é associado a imagem utilizada pelo grupo de instâncias. O script baixa os conteúdos da API do endereço de Gitlab https://gitlab.com/nicolasumaras/api_projeto_dev, instala o python e pip e por fim executa o gunicorn baseado nas configurações do arquivo gunicron.conf.py


## Melhorias

A principal melhoria que poderia ser implementada seria alterar a API para que os cometários fossem salvos em um banco de dados. Desta forma seria possível aumentar o número de instâncias e utilizar o load balancer de forma correta sem comprometer a experiência do cliente. Utilizando desta forma seria possível inclsuive partir para o Cloud Run ou App Engine ao invés de uma instância Compute na GCP. Isso poderia inclusive diminuir os custos de operação da API, uma vez que esses serviços cobram por execução. Essas soluções não foram utilizadas aqui devido a natureza statefull atual, as sugestões citadas necessitam que a aplicação seja stateless.
## Comissionamento
Apenas a pipeline do Gitlab é necessária para subir toda a infraestrutura, não sendo necessária nenhuma atuação manual dentro da GCP após a execução do script. A única atuação dentro da GCP é a criação do projeto e ativação da API Cloud Resource Manager, as demais APIs necessárias são habilitadas automaticamente pelo script 
